function Syncope(ele,url)
{
    this.pere = ele;
    this.url = url;
    
    this.delete = function()
    {
        ele = document.getElementById("syncope-fond");
        ele.parentNode.removeChild(ele);
    }
    
    this.create = function()
    {
        var fond = document.createElement("div");
        fond.id = 'syncope-fond';
        var contenu = document.createElement("div");
        contenu.id = 'syncope-contenu';
        fond.appendChild(contenu);
        this.pere.appendChild(fond);
        
        return contenu;
    }
    this.aff = function()
    {
        var contenu = this.create();

    
        xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() 
        {
            if (xhr.readyState == 4)
            {
                
                if(xhr.status == 200)
                {
                    contenu.innerHTML = xhr.responseText;  
                }
            }
        }  
        // cas de la méthode post
        xhr.open("GET",this.url,false); 
        xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded;charset=utf-8');
        xhr.send();
        
    }
}