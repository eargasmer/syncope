Syncope
=======
Syncope est un projet de fenètre modal en (50 lignes de) javascript pure (et une pointe de css).

Exemple
-------
pour créer une fenètre
``` javascript
popup = new Syncope(document.body,'content.html'); 
popup.aff();
```

Pour enlever la fenètre:
``` javascript
popup.delete();
```